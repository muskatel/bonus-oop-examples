# Craig's Bonus OOP Examples

## Reading Order:

1. Main_Simple
2. Main_Aggregation
3. Main_Association
4. Main_Inheritance
5. Main_Interfaces
6. Main_Polymorphism
7. Main_Inheritance_Advanced
8. Main_Interfaces_Advanced
9. Main_Delegates_Inheritance
10. Main_Delegates_Interfaces

---

Copyright 2022, Craig Marais (@muskatel)
