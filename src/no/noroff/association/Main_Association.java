package no.noroff.association;

public class Main_Association {

    public static void main(String[] args)
    {
        System.out.println("--- ASSOCIATION ---\n");

        // Let us create house and mailbox with association.
        // ┌─────────┐        ┌───────┐
        // │ Mailbox │────────│ House │
        // └─────────┘        └───────┘
        // The mailbox is "connected" to the house

        // A house with a mailbox
        Mailbox mailbox = new Mailbox("42 Universe Road");
        House house = new House();
        house.setMailbox(mailbox);

        // We can access the address direct from the mailbox or through the house object

        System.out.println(house);
        System.out.println("House: " + house.getHouseAddress());

        System.out.println(mailbox);
        System.out.println("Mailbox: " + mailbox.getAddress());

        System.out.println("\n--- --- ---\n");

        // A house without a mailbox

        House odd_house = new House();

        System.out.println(odd_house);
        System.out.println(odd_house.getHouseAddress());

        System.out.println("\n--- --- ---\n");
    }
}