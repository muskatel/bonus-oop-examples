package no.noroff.aggregation;

public class Main_Aggregation {

    public static void main(String[] args)
    {
        System.out.println("--- AGGREGATION ---\n");

        // Let us create house and mailbox with aggregation.
        // ┌─────────┐        ┌───────┐
        // │ Mailbox │──────<>│ House │
        // └─────────┘        └───────┘
        // The mailbox BELONGS to the house

        Mailbox mailbox = new Mailbox("27 Niels Jules Gate");

        // The house cannot be created without a mailbox
        House house =  new House(mailbox);

        System.out.println(house);
        System.out.println(mailbox);

        System.out.println(house.getAddress());

        System.out.println("--- --- ---\n");
    }
}