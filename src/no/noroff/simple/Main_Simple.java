package no.noroff.simple;

public class Main_Simple {

    public static void main(String[] args)
    {
        System.out.println("--- SIMPLE OBJECTS ---\n");

        // Consider a house ...
        //        _______
        //       /       \
        //      /_________\
        //      │ ╔╦╗  ╔═╗│
        //  ╤   │ ╠╬╣  ║.║│
        //  │   │ ╚╩╝  ║ ║│
        //
        // As you can see it has a nice mailbox next to it
        // ┌─────────┐        ┌───────┐
        // │ Mailbox │        │ House │
        // └─────────┘        └───────┘
        // The mailbox and the house are not connected

        Mailbox mailbox = new Mailbox("11 Inkognito gata");
        House house =  new House();

        // but these two classes know nothing about each other.

        System.out.println(house.toString());
        System.out.println(mailbox.toString()); // The .toString() is not needed, as it is implied

        // We must access the address only from the mailbox object

        System.out.println(mailbox.getAddress());

        System.out.println("\n--- --- ---\n");
    }
}