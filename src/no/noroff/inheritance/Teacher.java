package no.noroff.inheritance;

public class Teacher extends Person
{
    private String department;
    private int salary;

    public Teacher(String firstName, String lastName, String department, int salary)
    {
        super(firstName, lastName);
        this.department = department;
        this.salary = salary;
    }

    public String getDepartment(){
        return department;
    }

    protected int getSalary(){
        return salary;
    }


}
