package no.noroff.delegates_interfaces;

public class Card implements Payment
{
    private Float amount;
    private String currency;


    public Card(Float amount, String currency){
        this.amount = amount;
        this.currency = currency;
    }

    public void pay() {
        System.out.println("Paid "+ amount+ " " + currency+" by cash.");
    }
}
