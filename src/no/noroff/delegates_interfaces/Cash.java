package no.noroff.delegates_interfaces;

public class Cash implements Payment
{
    private Float amount;
    private String currency;


    public Cash(Float amount, String currency){
        this.amount = amount;
        this.currency = currency;
    }

    public void pay() {
        System.out.println("Paid "+ amount+ " " + currency+" by cash.");
    }
}
