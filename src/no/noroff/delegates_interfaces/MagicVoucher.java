package no.noroff.delegates_interfaces;

public class MagicVoucher implements Payment
{

    public void pay()
    {
        System.out.println("Paid with magic voucher cash! Money is for losers!");
    }
}
