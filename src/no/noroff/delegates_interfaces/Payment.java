package no.noroff.delegates_interfaces;

public interface Payment
{
    public void pay();
}
