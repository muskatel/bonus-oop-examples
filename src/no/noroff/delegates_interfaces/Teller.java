package no.noroff.delegates_interfaces;

public class Teller
{
    public Teller()
    {
        System.out.println("How may I help you today?");
    };

    public void handlePayment(Payment payment)
    {
        payment.pay();
    }
}
