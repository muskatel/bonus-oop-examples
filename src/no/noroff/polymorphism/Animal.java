package no.noroff.polymorphism;

public abstract class Animal
{
    // Animals can make noises (even foxes)
    public abstract void makeNoise();

    // Handy dandy info function!
    public String getInfo(){
        return "This is an Animal";
    }

}
