package no.noroff.delegates_inheritance;

public class Cash extends Payment
{
    public void pay() {
        System.out.println("Paid "+ amount+ " " + currency+" by cash.");
    }

    public Cash(Float amount, String currency){
        this.amount = amount;
        this.currency = currency;
    }
}
