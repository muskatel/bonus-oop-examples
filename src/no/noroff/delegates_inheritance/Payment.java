package no.noroff.delegates_inheritance;

public abstract class Payment
{
    protected Float amount;
    protected String currency;
    public abstract void pay();
}

