package no.noroff.delegates_inheritance;

public class Card extends Payment
{
    public void pay() {
        System.out.println("Paid "+ amount+ " " + currency+" by card.");
    }

    public Card(Float amount, String currency){
        this.amount = amount;
        this.currency = currency;
    }
}
