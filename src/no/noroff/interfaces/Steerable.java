package no.noroff.interfaces;

public interface Steerable
{
    public void steer();
    public void stop();
}
