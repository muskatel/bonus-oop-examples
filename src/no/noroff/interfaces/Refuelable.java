package no.noroff.interfaces;

public interface Refuelable
{
    public void refuel();
}
