package no.noroff.interfaces;

public class Trolley implements Steerable{
    public void steer()
    {
        System.out.println("A trolley is steered with a handle.");
    };
    public void stop()
    {
        System.out.println("A trolley stops when you stop pushing it.");
    };

    public String toString() {
        return "This is a trolley.";
    }
}

