package no.noroff.interfaces;

import java.util.ArrayList;

public class Main_Interfaces
{
    public static void main(String[] args)
    {
        System.out.println("--- INTERFACES ---\n");

        // We have a trolley and a car
        Trolley exampleTrolley = new Trolley();
        Car exampleCar = new Car();

        System.out.println("\nAccess each manually:\n");

        // both can be steered
        exampleTrolley.steer();
        exampleCar.steer();

        // and both can be stopped
        exampleTrolley.stop();
        exampleCar.stop();

        // We can store them in a collection of Steerable things ...
        ArrayList<Steerable> steerables = new ArrayList<Steerable>();
        steerables.add(exampleCar);
        steerables.add(exampleTrolley);

        System.out.println("\nUsing a range loop:\n");

        // We can access each in turn using a range loop
        for (Steerable thing: steerables)
        {
            thing.steer();
            thing.stop();
        }
    }
}
